import buble from 'rollup-plugin-buble';

export default {
	entry: 'index.js',
	format: 'iife',
	plugins: [
		
		buble()
	],
	dest: 'bin/bundle.js',
}


